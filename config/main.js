const config = {
    app: {
        port: process.env.PORT || 3000,
        queue_name: process.env.RITZI_QUEUE_NAME,
        mail_from_address: process.env.MAIL_FROM_ADDRESS,
        mail_from_name: process.env.MAIL_FROM_NAME
    },
    rabbit: {
        protocol: 'amqp',
        hostname: process.env.RABBITMQ_HOST || 'rabbit',
        port: process.env.RABBITMQ_PORT || 5672,
        username: process.env.RABBITMQ_USER,
        password: process.env.RABBITMQ_PASSWORD
    },
    smtp: {
        pool: true,
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        auth: {
            user: process.env.MAIL_USERNAME,
            pass: process.env.MAIL_PASSWORD
        },
        tls: {
            rejectUnauthorized: false
        }
    }
};

export default config;
