import {App} from "./src/app.js";
import path from "path";
import config from "./config/main.js";
import logger from "./src/components/Logger.js";

global.basePath = path.resolve();

let app = new App();
app.listen()
    .then(() => {
        logger.info(`Listening to port ${config.app.port}`)
    })
    .catch((e) => {
        logger.error(`Launch error ${e.message}`)
    });
