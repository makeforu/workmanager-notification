FROM node:15-alpine

WORKDIR /app
COPY package*.json ./
RUN npm install

ENTRYPOINT ["tail", "-f", "/dev/null"]
