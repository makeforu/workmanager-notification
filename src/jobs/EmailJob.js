import {Worker} from "../components/Worker.js";
import {Mailer} from "../components/Mailer.js";
import logger from "../components/Logger.js";

export class EmailJob extends Worker {

    constructor() {
        super(process.env.QUEUE_NOTIFICATION_EMAIL);
    }

    /**
     * Send email worker
     * @param msg
     * @returns {Promise<void>}
     */
    async run(msg) {
        try {
            let content = msg.content.toString();
            let data = JSON.parse(content);

            let mailer = await new Mailer(data.template, data.params);
            await mailer.send(data.email, data.subject);
        } catch (e) {
            logger.error(`Error run EmailJob ${e.message}`)
        }
    }
}
