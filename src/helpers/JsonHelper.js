export class JsonHelper {

    /**
     * Checking if the string is json
     *
     * @param {string} str
     * @returns {boolean}
     */
    static isJson(str)
    {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
}
