import {EmailJob} from "../jobs/EmailJob.js";

export class NotificationEmailService {

    /**
     * Listen queue email
     * @returns {Promise<void>}
     */
    async listen() {
        let job = new EmailJob();
        await job.consume();
    }
}
