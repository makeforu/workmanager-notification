import {NotificationEmailService} from "./services/NotificationEmailService.js";

export class App {

    constructor() {
        this.email = new NotificationEmailService();
    }

    async listen() {
        await this.email.listen();
    }
}
