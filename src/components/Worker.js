import amqp from "amqplib";
import config from "../../config/main.js";

export class Worker {

    constructor(queue_name) {
        this.queue_name = queue_name;
        this.options =  {
            arguments: {
                // 'x-max-priority': 10
            }
        };
    }

    /**
     * Publisher queue
     *
     * @param {string} msg
     * @returns {Promise<void>}
     */
    async publisher(msg) {
        const open = await amqp.connect(config.rabbit);
        const channel = await open.createChannel();

        channel.assertQueue(this.queue_name, this.options).then(async (ok) => {
            await channel.sendToQueue(this.queue_name, Buffer.from(msg));
        });
    }

    /**
     * Read messages from the queue
     *
     * @returns {Promise<any>}
     */
    async consume() {
        const open = await amqp.connect(config.rabbit);
        const channel = await open.createChannel();

        channel.assertQueue(this.queue_name, this.options)
            .then(async (ok) => {
                await channel.consume(this.queue_name, async (msg) => {
                    await this.run(msg)
                    await channel.ack(msg);
                })
            });
    }

    async run() {
        throw new Error('You have to implement the method run!');
    }
}
