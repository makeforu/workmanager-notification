import nodemailer from 'nodemailer';
import config from "../../config/main.js";
import {MailTemplate} from "./MailTemplate.js";

export class Mailer {

    constructor(template, params) {
        return new Promise((resolve) => {
            let mailTemplate = new MailTemplate(template, params);
            mailTemplate.render().then((content) => {
                this.from = config.app.mail_from_address;
                this.content = content;
                resolve(this);
            });
        })
    }

    /**
     * Send mail
     * @param to
     * @param subject
     * @returns {Promise<void>}
     */
    async send(to, subject) {
        const transporter = nodemailer.createTransport(config.smtp);
        await transporter.sendMail({
            from: this.from,
            to: to,
            subject: subject,
            html: this.content
        });
    }
}



