import fs from "fs";

export class MailTemplate {

    constructor(template, params) {
        this.params = params;
        this.template = `${basePath}/src/mails/${template}.html`;
    }

    async render() {
        let content = fs.readFileSync(this.template, 'utf-8');
        for (let key in this.params) {
            content = content.replace(`<render-tag>${key}</render-tag>`, this.params[key])
        }
        return content;
    }
}
